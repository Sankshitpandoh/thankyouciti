import Vue from 'vue'
import App from './App.vue'

import Router from 'vue-router'
import Thank from './components/Thank'

Vue.use(Router)


Vue.config.productionTip = false

const router = new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Thank },
  ]
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
